var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
	cleanCSS		= require('gulp-clean-css'),
	uglify			= require('gulp-uglify'),
    autoprefixer    = require('gulp-autoprefixer');

gulp.task('styles', function () {
    // Task for CSS
    gulp.src('./assets/sass/**/*.scss')
        .pipe(autoprefixer())
        .pipe(sass({
            outputStyle : 'compact'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', function() {
    gulp.watch('assets/sass/**/*.scss', ['styles']);
});

gulp.task('minify-css', function () {
	return gulp.src('./public/css/*.css')
		.pipe(cleanCSS({
			compatibility : 'ie8'
		}))
		.pipe(gulp.dest('./public/production'));
});

gulp.task('uglify', function () {
	return gulp.src('assets/js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('./public/production/js'));
});