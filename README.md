# APinkmage setup #

## Dependencies ##
1. jQuery <strong> 1.11.3 </strong>
2. jQuery UI <strong> 1.11.4 </strong>

## Setup the dropzone ##
~~~html
<div id="dropzone" class="dropzone"></div>
~~~

## Setup thumbnails into a div container ##
~~~html
<div id="thumb-picture" class="thumb-img">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
    <img src="path/to/image" class="thumbnail" id="thumbnail">
</div>
~~~

## Call the JS library / CSS files once the DOM is load ##
~~~html 
<script type="text/javascript">
// <![CDATA[
    $(document).ready(function () {
        new APinkmage();
    });
// ]]>
</script>
~~~
~~~html 
<link rel="stylesheet" href="path/to/apinkmage.css" type="text/css">    
~~~

# APinkmage work #
> 