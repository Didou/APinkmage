function APinkmage (user) {
	var $document 			= $(document);
	
	// Dropzone
	var dropzone 			= $('#dropzone');
	var dropzoneWidth 		= dropzone.width();
	var dropzoneHeight 		= dropzone.height();
	
	// Thumbnails
	var thumbPicture 		= $('#thumb-picture');
	var thumbnails			= $('#thumb-picture').children();
	var overlay;
	
	var dropItems			= [];
	var event_state 		= {};
	var $container;
	
	// Image
	var constrain 			= false;
	var min_width 			= 60;
    var min_height 			= 60;
    var max_width 			= 400;
    var max_height 			= 500;
	var original_width;
	var original_height;
	
	// Corbeille
	var corbeille;
	var containerRemove 	= {};
	var itemBeforeRemove 	= {};
	var canBeRemove 		= true;
	var beforeRemove;
	
	// Canvas Event
	var btnExport 			= $('#dropzone-export');
    var btnClearCanvas   	= $('#dropzone-clearcanvas');
	var btnClearDropzone 	= $('#dropzone-clearboard');
	var canvas;
	
	// Constante
	var FINAL_WIDTH 		= 1920;
	var FINAL_HEIGHT 		= 1080;
	
    // User
    var user            	= user;
    
	init = function () {
		_initThumbPicture();
		_initOverlay();
		_initCanvas();
		_initDropzone();
		_initRemoveItem();
		
		observeThumbPicture();
		observeDropzone();
		
		observeResizerHelper();
		observeDropItem();
		
		observeButtonHelper();
	};
	
	/****************************************************************/
	/******************* Init configuration *************************/
	/****************************************************************/
	_initThumbPicture = function () {
		var $child = thumbnails;
		var index = 0;
		
		$child.each(function(){
			$(this).css({
				width 	: '75px',
				height 	: '75px'
			}).attr('data-thumb', 'thumb-' + index).attr('id', 'thumb-' + index);
			
			index++;
		});
	}
	
	_initOverlay = function () {
		if ($('#overlay').length < 1) {
			$('body').prepend('<div class="overlay" id="overlay"><div>');
		}

		overlay = $('#overlay');
	};
	
	_initCanvas = function() {
		dropzone.wrapInner('<canvas id="canvas" width='+ dropzoneWidth +' height='+ dropzoneHeight +'></canvas>');
		
		canvas = document.getElementById('canvas');
	};
	
	_initRemoveItem = function () {
		var divCorbeille = $(document.createElement('div'));
		var iconCorbeille = $(document.createElement('span'));
		
		divCorbeille.attr({
			class 	: 'dropzone-remove-item',
			id		: 'dropzone-remove-item'
		});
		
		iconCorbeille.attr({
			class : 'remove-item'
		});
		
		dropzone.append(divCorbeille.append(iconCorbeille));
		corbeille = dropzone.find('div#dropzone-remove-item');
		
		containerRemove.left 	= corbeille.offset().left - corbeille.width();
		containerRemove.top  	= corbeille.offset().top;
		containerRemove.right 	= corbeille.offset().left + Math.round(corbeille.width()) - corbeille.width();
		containerRemove.bottom 	= corbeille.offset().top + Math.round(corbeille.height());
	};
	
	_initResizerHelper = function (element) {
		var style 	= element.attr('style');
		var height 	= element.height();
		var width 	= element.width();
		
		var parent  = $(document.createElement('div'));
		
		parent.css({
			position 	: 'absolute', 
			width 		: width + 'px',
			height 		: height + 'px',
			top 		: element.css('top'),
			left 		: element.css('left')
		}).addClass('drop-item resize-handler');
		
		element.wrap(parent);
		element.attr('style', '').css({
			position 	: 'relative',
			top			: 0,
			left		: 0,
			height 		: height + 'px',
			width 		: width + 'px',
			zIndex		: 1
		});
		
		_initCornerResizer(element);
	};
	
	_initCornerResizer = function (element) {
		var parent = element.parent();
		var div = jQuery(document.createElement('div'));
		
		parent.append("<span class='resizer ne-resizer' id='ne-resizer'></span>")
			   .append("<span class='resizer no-resizer' id='no-resizer'></span>")
			     .append("<span class='resizer se-resizer' id='se-resizer'></span>")
				  .append("<span class='resizer so-resizer' id='so-resizer'></span>");
	};
	
	_initDropzone = function () {
		dropzone.css({
			overflow : "hidden"
		});
	}
	
	/****************************************************************/
	/*******************       Helper       *************************/
	/****************************************************************/
	getPosition = function (e) {
		return {
			posX : e.pageX,
			posY : e.pageY
		};
	};
	
	countDroppedItem = function (e) {
		var childrenSize = dropzone.find('div.drop-item').length;
		
		return childrenSize > 1 ? childrenSize : 1;
	};
	
	toggleClassCorbeille = function (elem, classname) {
		if(elem.hasClass(classname)) {
			elem.removeClass(classname);
		} else {
			elem.addClass(classname);
		}
	};
	
	saveEventState = function(e, element){
		event_state.container_width = element.width();
		event_state.container_height = element.height();
		event_state.container_left = element.offset().left; 
		event_state.container_top = element.offset().top;
		
		event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft(); 
		event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
		event_state.evnt = e;
	};
	
	saveThumbDrop = function(e) {
		var element = $(e.target);
		
		itemBeforeRemove.width = element.width();
		itemBeforeRemove.height = element.height();
	};
	
	createFilename = function() {
        var user = user;
    };
    
    clearCanvas = function() {
        var canvas = document.getElementById('canvas');
		var ctx = canvas.getContext('2d');
		
		ctx.clearRect(0, 0, dropzoneWidth, dropzoneHeight);
	};
	
	clearDropzone = function() {
		var nodeChild = dropzone.children();
		
		nodeChild.each(function() {
			var node = $(this);
			
			if (node.hasClass('drop-item')) {
				node.remove();
			}
		});
	};
	
	getSignature = function () {
		
	};
	/****************************************************************/
	/*******************       Moves        *************************/
	/****************************************************************/
	startMoving = function(e) {
		var element = $(e.target), thumbDrop = element.parent();
		
		$container 		= element.parent();
		corbeille 		= dropzone.find('div#dropzone-remove-item');
		
		toggleClassCorbeille(corbeille, 'is-active');
		
		e.preventDefault();
		e.stopPropagation();
		
		saveEventState(e, thumbDrop);
		
		$document.on('mousemove', moving);
		$document.on('mouseup', endMoving);
	};
	
	moving = function(e){
		var mouse = {}, element = $(e.target);
		
		e.preventDefault();
		e.stopPropagation();
		
		mouse.x = (e.clientX || e.pageX) + $(window).scrollLeft();
		mouse.y = (e.clientY || e.pageY) + $(window).scrollTop();
		
		if(mouse.x > containerRemove.left && mouse.x < containerRemove.right && mouse.y > containerRemove.top && mouse.y < containerRemove.bottom) {
			if(canBeRemove) {
				startRemoveDropItem(e);
				canBeRemove = false;
			}
		} else {
			corbeille.removeClass('is-focus');
		}
		
		
		if($container.hasClass('dropzone')) {
			return true;
		}
		
		$container.offset({
			'left': mouse.x - ( event_state.mouse_x - event_state.container_left ),
			'top': mouse.y - ( event_state.mouse_y - event_state.container_top ) 
		});
	};
	
	endMoving = function(e, element) {
		var mouse = {};
		
		mouse.x = (e.clientX || e.pageX) + $(window).scrollLeft();
		mouse.y = (e.clientY || e.pageY) + $(window).scrollTop();
		
		e.preventDefault();
		e.stopPropagation();
	
		toggleClassCorbeille(corbeille, 'is-active');
		
		if(mouse.x > containerRemove.left && mouse.x < containerRemove.right && mouse.y > containerRemove.top && mouse.y < containerRemove.bottom) {
			removeDropItem(e);
		}	
		
		canBeRemove = true;
		
		$document.off('mouseup', endMoving);
		$document.off('mousemove', moving);
	};
	
	
	/****************************************************************/
	/*******************       Resize       *************************/
	/****************************************************************/
	startResize = function (e) {
		var element = $(e.target), thumbDrop = element.parent();
		
		$container = element.parent();
		
		original_width = $container.width();
		original_height = $container.height();
		
		e.preventDefault();
		e.stopPropagation();
		
		saveEventState(e, thumbDrop);
		
		$(document).on('mousemove', resizing);
		$(document).on('mouseup', endResize);
	};
	
	endResize = function (e) {
		e.preventDefault();
		
		$document.off('mouseup', endResize);
		$document.off('mousemove', resizing);
	};
	
	resizing = function (e) {
		var element = $(event_state.evnt.target), resizer = element.attr('id'), response = false;
		var mouse={}, width, height, left, top, offset = $container.offset();
		
		var img = $container.find('img');
		
		mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft(); 
		mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
		
		var containerWidth = $container.width();
		var containerHeight = $container.height();
	 	
		
		switch(resizer) {
			case 'no-resizer' : 
				width =  original_width - ((event_state.container_left - mouse.x) * -1);
				height = event_state.container_height - (mouse.y - event_state.container_top);
				left = mouse.x - ( event_state.mouse_x - event_state.container_left )
				top = mouse.y;
				response = true;
				break;
			case 'ne-resizer' : 
				width =  (event_state.container_left - mouse.x) * -1;
				height = event_state.container_height - (mouse.y - event_state.container_top);
				left = event_state.container_left;
				top = mouse.y;
				response = true;
				break;
			case 'so-resizer' : 
				width = event_state.container_width - (mouse.x - event_state.container_left);
				height = mouse.y  - event_state.container_top;
				left = mouse.x;
				top = event_state.container_top;
				response = true;
				break;
			case 'se-resizer' : 
				width = mouse.x - event_state.container_left;
				height = mouse.y  - event_state.container_top;
				left = event_state.container_left;
				top = event_state.container_top;
				response = true;
				break;
            default : 
				response = false;
                return false;
                break;
		}
		
		if (e.shiftKey) {
			height = width / original_width * original_height;
	  	}
		
		if(width > min_width && height > min_height && width < max_width && height < max_height && response == true) {
			$container.css({
				width : width + 'px',
				height : height + 'px'
			});

			img.css({
				width : width+ 'px',
				height : height + 'px'
			});

			$container.offset({'left': left, 'top': top});	
		}
	};
	
	/****************************************************************/
	/*******************       Remove       *************************/
	/****************************************************************/
	startRemoveDropItem = function (e) {
		var mouse = {}, element = $(e.target), thumbDrop = element.parent(), image = thumbDrop.find('img');
		
		mouse.x = (e.clientX || e.pageX) + $(window).scrollLeft();
		mouse.y = (e.clientY || e.pageY) + $(window).scrollTop();
		
		thumbDrop.css({
			width 	: '45px',
			height 	: '45px'
		});
		
		image.css({
			width 	: '45px',
			height 	: '45px'
		});
		
		corbeille.addClass('is-focus');
		$container.offset({
			'left': mouse.x - ( event_state.mouse_x - event_state.container_left ),
			'top': mouse.y - ( event_state.mouse_y - event_state.container_top ) 
		});
	};
	
	removeDropItem = function (e) {
		var target = $(e.target), thumbDrop = target.parent();
		
		if(thumbDrop.hasClass('drop-item')) {
			thumbDrop.remove();
		}
		
		corbeille.removeClass('is-focus');
		return false;
	};

	
	/****************************************************************/
	/*******************       Remove       *************************/
	/****************************************************************/
	drawPicture = function(e) {
		var ctx 			= canvas.getContext('2d'),
            itemDropped 	= dropzone.find('img');
		
        drawBackground(ctx, dropzoneWidth, dropzoneHeight);
        
		itemDropped.each(function () {
			var data    = {};
			var $this 	= $(this);
			var parent 	= $this.parent();
			
			data.x 		= parseInt(parent.css('left'));
			data.y 		= parseInt(parent.css('top'));
			data.width 	= parseInt($this.width());
			data.height = parseInt($this.height());
			
			var image = new Image(); 
			image.src = $this.attr('src');
			
			ctx.drawImage(image, data.x, data.y, data.width, data.height);
			parent.remove();
		});
		
		var EXPORT_PNG = resizeCanvas();
		exportCanvas(EXPORT_PNG);
	};
	
    drawBackground = function (canvas, width, height) {
        var backgroundSrc 	= dropzone.css('background-image');
		var backgroundUrl  	= backgroundSrc.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
		var backgroundImage = new Image();
        
        backgroundImage.src = backgroundUrl;
		backgroundImage.style.width = FINAL_WIDTH + 'px';
		backgroundImage.style.height = FINAL_HEIGHT + 'px';

		canvas.drawImage(backgroundImage, 0, 0, width, height);
    };
	
	resizeCanvas = function(canvas) {
		var ratioWidth = 0, ratioHeight = 0;
		var FINAL_CANVAS = document.createElement('canvas');
		var ctx = FINAL_CANVAS.getContext('2d');
		
		
		FINAL_CANVAS.style.width 	= FINAL_WIDTH + 'px';
		FINAL_CANVAS.style.height 	= FINAL_HEIGHT + 'px';
	
		ratioWidth 	= (parseInt(FINAL_WIDTH) / parseInt(dropzoneWidth)) * 100; 
		ratioHeight = (parseInt(FINAL_HEIGHT) / parseInt(dropzoneHeight)) * 100; 
		
		drawBackground(ctx, FINAL_WIDTH, FINAL_HEIGHT);
		
		return FINAL_CANVAS;
	};
	
	exportCanvas = function(canvas) {
		//var canvas 			= document.getElementById('canvas');
		var finalPicture64 	= canvas.toDataURL('image/png', 1); 
		var finalPicture8 	= canvas.toDataURL('image/png', 'image/octet-stream'); 
		var href			= document.createElement('a');
		
		href.setAttribute('id', 'btnDowload');
		href.setAttribute('download', 'didou.png');
		
		href.addEventListener('click', function() {
			var canvas 			= document.getElementById('canvas');
			var finalPicture64 	= canvas.toDataURL('image/png', 1); 
			href.href = finalPicture64;
		});
		
		href.click();
		window.open(finalPicture64);
		clearCanvas();
	};
	
	/****************************************************************/
	/******************* Listener thumbnail *************************/
	/****************************************************************/
	
	observeThumbPicture = function() {
		thumbnails.draggable({
			cursor: "grabbing",
			helper: "clone",
			containment : $('body'),
			connectToSortable: this.dropzone,
			zIndex : 100,

			start : function (e, ui) {
				overlay.addClass('is-active');
				dropzone.addClass('is-focus');
			},
			stop : function(e, ui) {
				overlay.removeClass('is-active');
				dropzone.removeClass('is-focus');
			}
		});
	};
	
	/****************************************************************/
	/******************* Listener thumbnail *************************/
	/****************************************************************/
	observeDropzone = function () {
		var canvas = document.getElementById('canvas');
		
		dropzone.droppable({
			drop : function (e, ui) {
				var elem  = $(ui.helper), clone = elem.clone(), mouse = {}, offset = {}, posX, posY;
				
				if(elem.hasClass('thumbnail')) {
					mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft(); 
					mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

					offset.top 	= Math.round(dropzone.offset().left);
					offset.left	= Math.round(dropzone.offset().top);

					posX = (Math.round(mouse.x) - Math.round(offset.top)) - Math.round(clone.height() / 2);
					posY = (Math.round(mouse.y) - Math.round(offset.left)) - Math.round(clone.height() / 2);

					var index = countDroppedItem();
					
					clone.attr({
						class 			: 'thumb-dropped',
						id 				: 'dropped-' + index,
						"data-thumb" 	: $(this).attr('data-thumb')
					});
					
					clone.css({
						position : 'absolute',
						top : posY + 'px',
						left : posX + 'px',
						cursor: 'move'
					});		

					$(this).prepend(clone);
					
					_initResizerHelper(clone);
					$container = clone.parent();
				}
			}
		});
		
	};
	
	/****************************************************************/
	/*****************  Listener drop item    ***********************/
	/****************************************************************/
	observeDropItem = function () {
		$document.on('mousedown', '.drop-item', startMoving);
	};
	
	observeResizerHelper = function () {
		$document.on('mousedown', '.resizer', startResize);
	};
	
	observeButtonHelper = function () {
		btnExport.on('click', drawPicture);
		btnClearCanvas.on('click', clearCanvas);
		btnClearDropzone.on('click', clearDropzone);
	};
	
	// Init 
	init();
}
